<?php

namespace App\Tests;

use App\Question\Domain\Entity\Question;
use App\Survey\Infrastructure\SurveyRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class RoutingTest extends WebTestCase
{
    public function testStart(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Математический тест');
    }

    public function testSubmitSuccess(): void
    {
        $client = static::createClient();

        /** @var SurveyRepository $rep */
        $rep = static::getContainer()->get(SurveyRepository::class);
        $survey = $rep->findLastSurvey();

        $questions = $survey->getQuestions();
        $json = [];

        foreach ($questions as $question) {
            foreach ($question->getAnswers() as $answer) {
                if ($answer->getQuestion()->getId()->toString() === $question->getId()->toString() && $answer->isCorrect) {
                    $json[$question->getId()->toString()][] = $answer->getId()->toString();
                }
            }
        }

        $client->request(Request::METHOD_POST, "/survey/{$survey->getId()}/submit",
            [
                'answers' => $json,
            ]
        );

        $this->assertResponseRedirects();
        $client->followRedirect();

        $client->getResponse();
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Вопросы на которые ответили верно (10)');
    }
}
