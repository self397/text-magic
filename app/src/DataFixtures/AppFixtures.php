<?php

namespace App\DataFixtures;

use App\Answer\Domain\Entity\Answer;
use App\Question\Domain\Entity\Question;
use App\Survey\Domain\Entity\Survey;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $survey = new Survey();
        $survey->name = 'Математический тест';
        $survey->setCreatedAt(new DateTimeImmutable('now'));
        $manager->persist($survey);

        $questionsData = $this->getQuestions();

        foreach ($questionsData as $qData) {
            $question = new Question();
            $question->question = $qData['question'];
            $question->setSurvey($survey);
            $question->setCreatedAt(new DateTimeImmutable('now'));
            $manager->persist($question);

            foreach ($qData['answers'] as $aData) {
                $answer = new Answer();
                $answer->answer = $aData['answer'];
                $answer->isCorrect = $aData['isCorrect'];
                $answer->setQuestion($question);
                $answer->setCreatedAt(new DateTimeImmutable('now'));
                $manager->persist($answer);
            }
        }

        $manager->flush();
    }

    private function getQuestions(): array
    {
        return [
            [
                'question' => '1 + 1 =',
                'answers' => [
                    ['answer' => '3', 'isCorrect' => false],
                    ['answer' => '2', 'isCorrect' => true],
                    ['answer' => '0', 'isCorrect' => false],
                ],
            ],
            [
                'question' => '2 + 2 =',
                'answers' => [
                    ['answer' => '4', 'isCorrect' => true],
                    ['answer' => '3 + 1', 'isCorrect' => true],
                    ['answer' => '10', 'isCorrect' => false],
                ],
            ],
            [
                'question' => '3 + 3 =',
                'answers' => [
                    ['answer' => '1 + 5', 'isCorrect' => true],
                    ['answer' => '1', 'isCorrect' => false],
                    ['answer' => '6', 'isCorrect' => true],
                    ['answer' => '2 + 4', 'isCorrect' => true],
                ],
            ],
            [
                'question' => '4 + 4 =',
                'answers' => [
                    ['answer' => '8', 'isCorrect' => true],
                    ['answer' => '4', 'isCorrect' => false],
                    ['answer' => '0', 'isCorrect' => false],
                    ['answer' => '0 + 8', 'isCorrect' => true],
                ],
            ],
            [
                'question' => '5 + 5 =',
                'answers' => [
                    ['answer' => '6', 'isCorrect' => false],
                    ['answer' => '18', 'isCorrect' => false],
                    ['answer' => '10', 'isCorrect' => true],
                    ['answer' => '9', 'isCorrect' => false],
                    ['answer' => '0', 'isCorrect' => false],
                ],
            ],
            [
                'question' => '6 + 6 =',
                'answers' => [
                    ['answer' => '3', 'isCorrect' => false],
                    ['answer' => '9', 'isCorrect' => false],
                    ['answer' => '0', 'isCorrect' => false],
                    ['answer' => '12', 'isCorrect' => true],
                    ['answer' => '5 + 7', 'isCorrect' => true],
                ],
            ],
            [
                'question' => '7 + 7 =',
                'answers' => [
                    ['answer' => '5', 'isCorrect' => false],
                    ['answer' => '14', 'isCorrect' => true],
                ],
            ],
            [
                'question' => '8 + 8 =',
                'answers' => [
                    ['answer' => '16', 'isCorrect' => true],
                    ['answer' => '12', 'isCorrect' => false],
                    ['answer' => '9', 'isCorrect' => false],
                    ['answer' => '5', 'isCorrect' => false],
                ],
            ],
            [
                'question' => '9 + 9 =',
                'answers' => [
                    ['answer' => '18', 'isCorrect' => true],
                    ['answer' => '9', 'isCorrect' => false],
                    ['answer' => '17 + 1', 'isCorrect' => true],
                    ['answer' => '2 + 16', 'isCorrect' => true],
                ],
            ],
            [
                'question' => '10 + 10 =',
                'answers' => [
                    ['answer' => '0', 'isCorrect' => false],
                    ['answer' => '2', 'isCorrect' => false],
                    ['answer' => '8', 'isCorrect' => false],
                    ['answer' => '20', 'isCorrect' => true],
                ],
            ],
        ];
    }
}
