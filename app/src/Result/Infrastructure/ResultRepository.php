<?php

namespace App\Result\Infrastructure;

use App\Result\App\Dto\ResultDto;
use App\Result\Domain\Entity\Result;
use App\Result\Domain\Interface\ResultRepositoryInterface;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

/**
 * @extends ServiceEntityRepository<Result>
 */
class ResultRepository extends ServiceEntityRepository implements ResultRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Result::class);
    }


    public function saveResult(ResultDto $resultDto): void
    {
        $result = new Result();

        $result->setSurvey($resultDto->survey);
        $result->correctAnswers = $resultDto->correct;
        $result->wrongAnswers = $resultDto->incorrect;
        $result->setCreatedAt(new DateTimeImmutable('now'));

        $this->getEntityManager()->persist($result);
        $this->getEntityManager()->flush();
    }

    public function getResultBySurvey(UuidInterface $survey): Result
    {
        $result = $this->findOneBy(criteria: ['survey' => $survey], orderBy: ['createdAt' => 'desc']);

        if (empty($result)) {
            throw new \Exception("Опрос не найден");
        }

        return $result;
    }

}
