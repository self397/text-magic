<?php

namespace App\Result\Domain\Interface;

use App\Result\App\Dto\ResultDto;
use App\Result\Domain\Entity\Result;
use Ramsey\Uuid\UuidInterface;

interface ResultRepositoryInterface
{
    public function saveResult(ResultDto $resultDto): void;

    public function getResultBySurvey(UuidInterface $survey): Result;
}