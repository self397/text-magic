<?php

namespace App\Result\App\Dto;

use App\Survey\Domain\Entity\Survey;

class ResultDto
{
    public function __construct(
        public array $correct,
        public array $incorrect,
        public Survey $survey
    )
    {

    }
}