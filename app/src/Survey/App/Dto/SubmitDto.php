<?php

namespace App\Survey\App\Dto;

use Ramsey\Uuid\UuidInterface;

class SubmitDto
{
    public function __construct(
        public UuidInterface $uuid,
        public array $answers
    )
    {

    }
}