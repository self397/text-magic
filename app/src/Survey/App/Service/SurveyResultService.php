<?php

namespace App\Survey\App\Service;

use App\Answer\Domain\Entity\Answer;
use App\Question\Domain\Entity\Question;
use App\Result\App\Dto\ResultDto;
use App\Result\Domain\Interface\ResultRepositoryInterface;
use App\Survey\App\Dto\SubmitDto;
use App\Survey\Domain\Entity\Survey;
use App\Survey\Domain\Interface\SurveyRepositoryInterface;
use App\Survey\Domain\Interface\SurveyResultInterface;

class SurveyResultService implements SurveyResultInterface
{
    public function __construct(
        private readonly SurveyRepositoryInterface $surveyRepository,
        private readonly ResultRepositoryInterface $resultRepository,
    )
    {

    }

    public function checkResultAnswers(SubmitDto $submitDto): void
    {
        $survey = $this->surveyRepository->findByUuid($submitDto->uuid);
        $answers = $submitDto->answers;

        $wrongToQuestion = [];
        $correctToQuestion = [];

        foreach ($survey->getQuestions() as $question) {
            $questionId = $question->getId()->toString();
            if (isset($answers[$questionId])) {
                if ($this->hasIncorrectAnswer($question, $answers[$questionId])) {
                    $wrongToQuestion[] = $question->question;
                    continue;
                }
                $correctToQuestion[] = $question->question;
            } else {
                $wrongToQuestion[] = $question->question;
            }
        }

        $this->resultRepository->saveResult(new ResultDto(correct: $correctToQuestion, incorrect: $wrongToQuestion, survey: $survey));
    }

    private function hasIncorrectAnswer(Question $question, array $submittedAnswers): bool
    {
        foreach ($submittedAnswers as $answer) {
            $foundAnswer = $this->findAnswerById($question, $answer);
            if (!$foundAnswer || !$foundAnswer->isCorrect) {
                return true;
            }
        }
        return false;
    }

    private function findAnswerById(Question $question, string $answerId): ?Answer
    {
        return $question->getAnswers()->filter(function($item) use ($answerId) {
            return $item->getId() == $answerId;
        })->first();
    }
}