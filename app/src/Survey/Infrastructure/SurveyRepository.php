<?php

namespace App\Survey\Infrastructure;

use App\Survey\Domain\Entity\Survey;
use App\Survey\Domain\Interface\SurveyRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

/**
 * @extends ServiceEntityRepository<Survey>
 */
class SurveyRepository extends ServiceEntityRepository implements SurveyRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Survey::class);
    }

    /**
     * @throws \Exception
     */
    public function findLastSurvey(): Survey
    {
        $survey = $this->findAll();

        if (empty($survey)) {
            throw new \Exception("Опрос не найден");
        }

        return current($survey);
    }

    public function findByUuid(UuidInterface $id): Survey{
        $survey = $this->find($id);

        if (empty($survey)) {
            throw new \Exception("Опрос не найден");
        }

        return $survey;
    }
}
