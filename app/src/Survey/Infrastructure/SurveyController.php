<?php

namespace App\Survey\Infrastructure;

use App\Result\Domain\Interface\ResultRepositoryInterface;
use App\Survey\App\Dto\SubmitDto;
use App\Survey\Domain\Interface\SurveyRepositoryInterface;
use App\Survey\Domain\Interface\SurveyResultInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class SurveyController extends AbstractController
{
    public function __construct(
        private readonly SurveyRepositoryInterface $repository,
        private readonly SurveyResultInterface $surveyResult,
        private readonly ResultRepositoryInterface $resultRepository
    )
    {
    }

    #[Route('/', name: 'survey_show')]
    public function show(): Response
    {
        $survey = $this->repository->findLastSurvey();

        return $this->render('survey/show.html.twig', [
            'survey' => $survey,
        ]);
    }

    #[Route('/survey/{id}/submit', name: 'survey_submit', methods: ['POST'])]
    public function submit(string $id, Request $request): Response
    {
        $this->surveyResult->checkResultAnswers(
            new SubmitDto(Uuid::fromString($id),
                $request->request->all('answers')
            )
        );

        return $this->redirectToRoute('survey_result', ['id' => $id]);
    }

    #[Route('/survey/{id}/result', name: 'survey_result', methods: ['GET'])]
    public function result(string $id): Response
    {
        $result = $this->resultRepository->getResultBySurvey(Uuid::fromString($id));

        return $this->render('result/result.html.twig', [
            'result' => $result,
        ]);
    }
}