<?php

namespace App\Survey\Domain\Interface;

use App\Survey\Domain\Entity\Survey;
use Ramsey\Uuid\UuidInterface;

interface SurveyRepositoryInterface
{
    public function findLastSurvey(): Survey;

    public function findByUuid(UuidInterface $id): Survey;
}