<?php

namespace App\Survey\Domain\Interface;

use App\Survey\App\Dto\SubmitDto;

interface SurveyResultInterface
{
    public function checkResultAnswers(SubmitDto $submitDto): void;
}