<?php

namespace App\Survey\Domain\Entity;

use App\Question\Domain\Entity\Question;
use App\Survey\Infrastructure\SurveyRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: SurveyRepository::class)]
class Survey
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'uuid', unique: true)]
    private UuidInterface $id;

    #[ORM\Column(length: 200)]
    public ?string $name = null;

    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column]
    private DateTimeImmutable $createdAt;

    #[ORM\OneToMany(targetEntity: Question::class, mappedBy: 'survey')]
    private Collection $questions;

    public function __construct(?UuidInterface $id = null)
    {
        $this->id = $id ?: Uuid::uuid4();
        $this->questions = new ArrayCollection();
    }

    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function setId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
